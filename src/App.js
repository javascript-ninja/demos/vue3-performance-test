import Calendar from './components/Calendar';

export default {
  name: 'App',
  components: {
    Calendar
  },
  data() {
    return {
      msg: 'Vue Performance Test'
    };
  },
  template: `
    <div id="app">
      <h2>{{ msg }}</h2>
      <Calendar />
    </div>
  `
};
