import Vue2 from '../node_modules/vue2/dist/vue.min.js';
import * as Vue3 from '../node_modules/vue/dist/vue.esm-browser.prod.js';
import App from './App';

new Vue2({
  el: '#app-v2',
  template: '<App/>',
  components: { App }
});
window.Vue2 = Vue2;

Vue3.createApp().mount(App, '#app-v3');
window.Vue3 = Vue3;
