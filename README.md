# Vue 3 Performance Test

Based on work by [Michael Petrosyan](https://itnext.io/angular-5-vs-react-vs-vue-6b976a3f9172)

### For running demo

- Clone https://github.com/vuejs/vue-next
- Run `yarn install && yarn build` to build `vue-next`
- `cd packages/vue`
- `yarn link`
- Clone this repository
- `yarn link vue`
- `yarn`

This repository requires `yarn` as it uses aliasing to keep Vue2 and Vue3 at the same time

## serve with hot reload at localhost:1234

`yarn dev`

# build for production with minification

`yarn build`
